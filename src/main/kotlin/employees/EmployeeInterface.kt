package employees

// Interface -> serves like a contract
interface EmployeeInterface {
    var firstName: String
    var lastName:  String
    fun doSomething(): String
}