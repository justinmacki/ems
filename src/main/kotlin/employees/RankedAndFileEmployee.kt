package employees

import java.time.LocalDate

class RankAndFileEmployee(
    firstName: String,
    lastName: String,
    department: String,
    hiringDate: LocalDate,
    location: LocationDetails
): Employee(firstName, lastName, department, hiringDate, location){
    var isRankedAndFile = true
    var isAdmin = false
}