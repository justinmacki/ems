package employees

import java.time.LocalDate

/**
 * This class should have the following attributes:
 * firstName
 * lastName
 * department
 * hiringDate - (cannot be modified)
 */

abstract class Employee(
    override var firstName: String,
    override var lastName: String,
    var department: String,
    val hiringDate: LocalDate,
    var location: LocationDetails
        ) : EmployeeInterface {
    var employeeID: String = ""

    override fun doSomething(): String = "Sample String"

    fun showEmployeeData(): String{
        return "Employee Name: $firstName $lastName from $department departnment."
    }//12 345 678

    init {
        this.employeeID = "${firstName.substring(0,2).uppercase()}-${lastName.substring(0,2).uppercase()}-${(10000000 until 99999999).random()}"
        println(employeeID)
    }
}

class Person(override var firstName: String,
             override var lastName: String): EmployeeInterface{
    override fun doSomething(): String = "Sample String"
}

