package employees

import java.time.LocalDate

class AdminEmployee(
    firstName: String,
    lastName: String,
    department: String,
    hiringDate: LocalDate,
    location: LocationDetails
): Employee(firstName, lastName, department, hiringDate, location){
    var isRankedAndFile = false
    var isAdmin = true
}