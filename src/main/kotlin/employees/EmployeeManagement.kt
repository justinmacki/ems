package employees

import java.time.LocalDate

fun main(){
    val address1 = LocationDetails("Makati", "Metro Manila", "NCR", "1124 XYZ Building, Legaspi Village")
    val employee1 = RankAndFileEmployee("Justin", "Macki", "IT", LocalDate.now(),address1)
    //val employee2 = RankAndFileEmployee("Justin", "Macki", "IT", LocalDate.now())

    //val employee2 = AdminEmployee("Brandon", "Cruz", "HR", LocalDate.now())


    val address2 = address1.copy(
        exactAddress = "1623 HJK Building, Salcedo Village"
    )

    println(employee1.location)

    // Higher Order Function
    // with, apply, run, let

//    with(employee2) {
//        println(firstName)
//        println(lastName)
//        println(department)
//        println(isRankedAndFile)
//        println(isAdmin)
//        println(employeeID)
//    }


}