package employees

/**
 * A class containing all the details obout the office location of an employee
 * City, Province, Region, Exact Address
 */

// Data Class -> automatic string conversion (toString())
// equal() checks the actual data
// we can use copy()
data class LocationDetails(
    var city: String,
    var province: String,
    var region: String,
    var exactAddress: String
)